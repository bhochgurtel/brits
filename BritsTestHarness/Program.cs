﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rti.common.office.excel.parse;
using System.Data;
using RTI_BritsBaseLibraries;

namespace BritsTestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            //load the meta data file
            LoadExcel myMetaData = new LoadExcel(LoadExcel.excelFileType.xlsx, "c:\\projects\\brits_data\\WellMetaData2.xlsx");
            DataSet myMetaDataSet = myMetaData.CreateDataSet();
            string connectionstring = "Server=meramac\\bdh;Database=BritsDatabase;User Id=Brits;Password=BritsRocks99!";
            MetaData myLoadMd = new MetaData(connectionstring, "PRRIP", myMetaDataSet);
            myLoadMd.CreateViewForTimeSeriesMetaData();
            //LoadMetaData myLoadMd = new LoadMetaData(connectionstring,"PRRIP",myMetaDataSet);
            //string view = myLoadMd.getMetaDataColumnsForView();
            //DataSet myDs = myLoadMd.getMetaDataDs("Sheet1");


            LoadExcel myTsData = new LoadExcel(LoadExcel.excelFileType.xlsx, "c:\\projects\\brits_data\\TestTimeSeries.xlsx");
            DataSet myTimeDs = myTsData.CreateDataSet();

            foreach (DataTable timeseries in myTimeDs.Tables)
            {
                TimeSeries myLoadTS = new TimeSeries(connectionstring,"PRRIP","PRRIP",timeseries.TableName,timeseries);
                myLoadTS.PopulateBulkDataTable();
                myLoadTS.CreateViewForTimeSeries();
            }
        }
    }
}
