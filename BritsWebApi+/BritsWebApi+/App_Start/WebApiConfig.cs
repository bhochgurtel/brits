﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BritsWebApi_
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "getTimeSeriesNames1",
                routeTemplate: "api/{tsmetadata}/{groupName}",
                defaults: new { controller = "TSMetaData" }
            );

            config.Routes.MapHttpRoute(
                name: "getMetaData1",
                routeTemplate: "api/{tsmetadata}/{groupName2}",
                defaults: new { controller = "TSMetaData" }
            );
        }
    }
}
