﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using rti.common.cs.db.SqlServer;
using System.Configuration;
using System.Data;

namespace BritsWebApi_.Models
{
    public class TimeSeries
    {
        public virtual DataSet getData(string timeseriesName)
        {
            string query = "select * from \"vw__timeseries_" + timeseriesName + "\" order by Date_Time";
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.Text, query));
        }

        public virtual DataSet getData(string timeseriesName, string startDate)
        {
            DateTime start = DateTime.Parse(startDate);
            string query = "select * from \"vw__timeseries_" + timeseriesName + "\" where Date_Time >= '" + startDate + "' order by Date_Time";
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.Text, query));
        }

        public virtual DataSet getData(string timeseriesName, string startDate, string endDate)
        {
            DateTime start = DateTime.Parse(startDate);
            DateTime end = DateTime.Parse(endDate);

            string query = "select * from \"vw__timeseries_" + timeseriesName + "\" where Date_Time >= '" + startDate + "' and Date_Time <= '" + endDate + "' order by Date_Time";
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.Text, query));
        }

       
    }
}