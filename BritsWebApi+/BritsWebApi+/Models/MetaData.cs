﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using rti.common.cs.db.SqlServer;
using System.Configuration;

namespace BritsWebApi_.Models
{
    public class MetaData
    {
        

        /// <summary>
        /// Return all meta data for a particular group
        /// </summary>
        /// <param name="groupName">The group a timeseries has been assigned to</param>
        /// <returns>All meta data for this group</returns>
        public virtual DataSet getMetaData(string groupName)
        {
            string query = "select * from vw_meta_" + groupName;
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.Text, query));
        }

        /// <summary>
        /// Return meta data for a particular group with filter names
        /// </summary>
        /// <param name="groupName">The group a timeseries has been assigned to</param>
        /// <param name="filterColumn">the column you wish to filter values by</param>
        /// <param name="filterValue">The value to go with the filter.</param>
        /// <returns>All meta data for this group</returns>
        public virtual DataSet getMetaData(string groupName,string filterColumn, string filterValue)
        {
            string query = "select * from vw_meta_" + groupName + " where " +filterColumn + "='" + filterValue +"'";
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.Text, query));
        }



        public virtual DataSet getGroups()
        {
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.StoredProcedure, "sproc_selectTSGroups", null));
        }

        public virtual DataSet getTimeSeriesByGroup(string groupName)
        {
            SqlParameter[] myParams = new SqlParameter[1];
            myParams[0] = new SqlParameter("@groupName", SqlDbType.NVarChar);
            myParams[0].Value = groupName;
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.StoredProcedure, "sproc_getTimeSeriesByGroup", myParams));
        }

        public virtual DataSet getTimeSeriesDateRange(string timeSeriesName)
        {
            SqlParameter[] myParams = new SqlParameter[1];
            myParams[0] = new SqlParameter("@timeSeriesName", SqlDbType.NVarChar);
            myParams[0].Value = timeSeriesName;
            return (SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["britsDB"].ToString(), CommandType.StoredProcedure, "sproc_GetDateRangeForTimeSeries", myParams));
        }

    }
}