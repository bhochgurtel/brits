﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Web.Mvc;
using System.Data;


namespace BritsWebApi_.Controllers
{
    public class TSMetaDataController : ApiController
    {
        public DataSet GetAllGroups()
        {
            BritsWebApi_.Models.MetaData myMetaData = new BritsWebApi_.Models.MetaData();
            return (myMetaData.getGroups());
        }

        public DataSet GetGroups(string groupName)
        {
            BritsWebApi_.Models.MetaData myMetaData = new BritsWebApi_.Models.MetaData();
            return (myMetaData.getTimeSeriesByGroup(groupName));
        }

        public DataSet GetMetaData(string groupName2)
        {
            BritsWebApi_.Models.MetaData myMetaData = new BritsWebApi_.Models.MetaData();
            return (myMetaData.getMetaData(groupName2));
        }

        public DataSet GetMetaData(string groupname, string column, string value)
        {
            BritsWebApi_.Models.MetaData myMetaData = new BritsWebApi_.Models.MetaData();
            return (myMetaData.getMetaData(groupname, column, value));
        }

        public DataSet GetDateRange(string timeSeriesName)
        {
            BritsWebApi_.Models.MetaData myMetaData = new BritsWebApi_.Models.MetaData();
            return (myMetaData.getTimeSeriesDateRange(timeSeriesName));
        }


    }
}
