﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Web.Mvc;
using System.Data;

namespace BritsWebApi_.Controllers
{
    public class TimeSeriesController : ApiController
    {
        public DataSet GetTimeSeries(string timeseriesName)
        {
            BritsWebApi_.Models.TimeSeries myMetaData = new BritsWebApi_.Models.TimeSeries();
            return (myMetaData.getData(timeseriesName));
        }

        public DataSet GetTimeSeries(string timeseriesName,string startDate)
        {
            BritsWebApi_.Models.TimeSeries myMetaData = new BritsWebApi_.Models.TimeSeries();
            return (myMetaData.getData(timeseriesName,startDate));
        }

        public DataSet GetTimeSeries(string timeseriesName, string startDate, string endDate)
        {
            BritsWebApi_.Models.TimeSeries myMetaData = new BritsWebApi_.Models.TimeSeries();
            return (myMetaData.getData(timeseriesName, startDate,endDate));
        }


    }
}
