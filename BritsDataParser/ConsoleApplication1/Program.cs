﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using System.IO;
using System.Net;

namespace ConsoleApplication1
{
    class Program
    {
   
        static void Main(string[] args)
        {
            ClientContext context = new ClientContext(@"http://fern");
            //var list = context.Web.Lists.GetByTitle("ParseBritsData");
            //var query = CamlQuery.CreateAllItemsQuery(10000);
            //var result = list.GetItems(query);

            //context.Load(result, items => items.Include(
            // item => item["Title"],
            // item => item["FileRef"]
            // ));

            //context.ExecuteQuery();

            //var item2 = result.First();

            ////get the URL of the file you want:
            //var fileRef = item2["FileRef"];

            ////get the file contents:
            //FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, fileRef.ToString());

            //using (var memory = new MemoryStream())
            //{
            //    byte[] buffer = new byte[1024 * 64];
            //    int nread = 0;
            //    while ((nread = fileInfo.Stream.Read(buffer, 0, buffer.Length)) > 0)
            //    {
            //        memory.Write(buffer, 0, nread);
            //    }
            //    memory.Seek(0, SeekOrigin.Begin);
            //    // ... here you have the contents of your file in memory, 
            //    // do whatever you want
            //}
            
            
            DownloadFile(context, "/ParseBritsData/", "c:\\projects\\", "DavesTry22.xls");
            
            
            
            //FileInformation fInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, "/ParseBritsData/TestTimeSeries.xlsx");
            //System.IO.FileStream outPutFile = System.IO.File.OpenWrite("c:\\projects\\test.xlsx");
            //fInfo.Stream.CopyTo(outPutFile);
            //fInfo.Stream.Close();
            //outPutFile.Close();
        }

        public static bool DownloadFile(ClientContext context, string fileRelativeUrl, string destinationPath, string fileName)
        {
            try
            {
                //fileRelativeUrl = fileRelativeUrl.Replace("/Images/", "/PublishingImages/");
                string sourceUrl = fileRelativeUrl + fileName;
                string completeDestinationPath = destinationPath + fileName;

                if (!System.IO.File.Exists(completeDestinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                    FileInformation spFileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, sourceUrl);

                    using (Stream destination = System.IO.File.Create(completeDestinationPath))
                    {
                        for (int a = spFileInfo.Stream.ReadByte(); a != -1; a = spFileInfo.Stream.ReadByte())
                            destination.WriteByte((byte)a);
                    }
                }
            }
            catch (Exception ex)
            {
                //Logger.WriteLog(string.Format("Error during copying file: {0} with error {1}", fileRelativeUrl, ex.StackTrace));
                return false;
            }
            return true;
        }

    }
}
