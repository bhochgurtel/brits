﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using Excel;
using System.Data;
using System.IO;
using rti.common.office.excel.parse;
using RTI_BritsBaseLibraries;

namespace BritsDataParser
{
    class CallSharePoint
    {
        protected string _spSiteUrl = string.Empty;
        protected string _documentLibraryName = "ParseBritsData";
        private ClientContext _context = null;
        protected string _tempFilePath = string.Empty;
        protected string _query = "<View><Query><Where><Eq><FieldRef Name='ParseComplete'></FieldRef><Value Type='Integer'>0</Value></Eq></Where></Query></View>";
        protected string _groupName = string.Empty;
        private string _connectionstring = string.Empty;

        public CallSharePoint(string siteUrl)
        {
            _spSiteUrl = siteUrl;
            _context = new ClientContext(_spSiteUrl);
        }

        public CallSharePoint(string siteUrl, string groupName, string connectionString,string tempFile,string documentLibraryName)
        {
            _spSiteUrl = siteUrl;
            _context = new ClientContext(_spSiteUrl);
            _groupName = groupName;
            _connectionstring = connectionString;
            _documentLibraryName = documentLibraryName;
            _tempFilePath = tempFile;
        }

        protected string ConnectionString
        {
            get { return _connectionstring; }
            set { _connectionstring = value; }
        }
       
        protected string CamlQuery
        {
            get { return _query; }
            set { _query = value; }
        }

        protected string TempFilePath
        {
            get { return _tempFilePath; }
            set { _tempFilePath = value; }
        }
        
        //private int _listId = null;

        protected ClientContext Context
        {
            get { return _context; }
            set { _context = value; }
        }


        protected string SpSiteUrl
        {
            get { return _spSiteUrl; }
            set { 
                  _spSiteUrl = value;
                  _context = new ClientContext(_spSiteUrl);
                }
        }

        public virtual void DoWork()
        {
            ListItemCollection items = FindParserWork();
            ProcessResults(ref items);
        }
        
        
        
        
        
        public virtual ListItemCollection FindParserWork()
        {
            ClientContext context = new ClientContext(_spSiteUrl);
            List myDocumentLibrary = context.Web.Lists.GetByTitle(_documentLibraryName);
            CamlQuery query = new CamlQuery();
            query.ViewXml = _query;
            ListItemCollection items = myDocumentLibrary.GetItems(query);
            context.Load(items);
            context.ExecuteQuery();
            return items;
        }

        public virtual void ProcessResults(ref ListItemCollection items)
        {
            string error = string.Empty;
            int id = 0;

            foreach (ListItem listItem in items)
            {
                if (listItem.FileSystemObjectType.ToString().ToLower() == "file")
                {
                    string errorText = "none";
                    bool blError = false;
                    try
                    {
                        string path = listItem["FileRef"].ToString();
                        id = Convert.ToInt16(listItem["ID"]);
                        DataSet test = DownloadFile(_context, path);
                        errorText =  PopulateDatabase(ref test);
                    } catch (Exception e)
                    {
                        errorText += e.ToString();
                        blError = true;
                    }

                    UpdateDocumentLibraryStatus(id, blError, errorText);
                }
            }
        }


        protected virtual string PopulateDatabase(ref DataSet parsedData)
        {
            StringBuilder errors = new StringBuilder();

            foreach (DataTable tsData in parsedData.Tables)
            {
                if (!tsData.TableName.ToLower().Contains("sheet"))
                {
                    TimeSeries myLoadTS = new TimeSeries(_connectionstring, _groupName, _groupName, tsData.TableName, tsData);
                    errors.Append(myLoadTS.PopulateBulkDataTable());
                    myLoadTS.CreateViewForTimeSeries();
                }
            }

            return errors.ToString();
        }

        protected virtual DataSet DownloadFile(ClientContext context, string fileRelativeUrl)
        {

            //context.Web.GetFileByServerRelativeUrl(fileRelativeUrl);
            
            DataSet result = new DataSet();
            try
            {
                FileInformation spFileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, fileRelativeUrl);
 
                using(FileStream myStream = System.IO.File.Create(_tempFilePath))
                {
                    for (int a = spFileInfo.Stream.ReadByte(); a != -1; a = spFileInfo.Stream.ReadByte())
                        myStream.WriteByte((byte)a);
                }

                LoadExcel myTsData = new LoadExcel(LoadExcel.excelFileType.xlsx, _tempFilePath);
                result = myTsData.CreateDataSet();
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex); 
            }

            return result;
        }
  

        protected virtual void UpdateDocumentLibraryStatus(int id,bool error, string errorText)
        {
 
            List oList = _context.Web.Lists.GetByTitle(_documentLibraryName);
            ListItem oListItem = oList.GetItemById(id);
            
         
            oListItem["ParseComplete"] = 1;
            oListItem["ParseDateComplete"] = DateTime.Now;

            if (error)
                oListItem["Error"] = 0;
            else
                oListItem["Error"] = 1;

            if (errorText != string.Empty)
                oListItem["ErrorOutput"] = errorText;

            oListItem.Update();

            _context.ExecuteQuery(); 
        }

    }
}
