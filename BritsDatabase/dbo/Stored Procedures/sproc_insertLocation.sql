﻿CREATE PROCEDURE [dbo].sproc_insertLocation
(
	@LocationType nvarchar(50),
	@LocationUnits nvarchar(50),
	@Latitute nvarchar(50),
	@Longitute nvarchar(50),
	@X nvarchar(50),
	@Y nvarchar(50)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [LocationId] ([LocationType], [LocationUnits], [Latitute], [Longitute], [X], [Y]) VALUES (@LocationType, @LocationUnits, @Latitute, @Longitute, @X, @Y);
	
SELECT ID, LocationType, LocationUnits, Latitute, Longitute, X, Y FROM LocationId WHERE (ID = SCOPE_IDENTITY())