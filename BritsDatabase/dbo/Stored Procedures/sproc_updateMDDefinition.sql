﻿
CREATE PROCEDURE [dbo].sproc_updateMDDefinition
(
	@mdid int,
	@columnName nvarchar(50),
	@value nvarchar(MAX),
	@sortX int,
	@sortY int,
	@Original_id int,
	@id int
)
AS
	SET NOCOUNT OFF;
UPDATE [MDDefinition] SET [mdid] = @mdid, [columnName] = @columnName, [value] = @value, [sortX] = @sortX, [sortY] = @sortY WHERE (([id] = @Original_id));
	
SELECT id, mdid, columnName, value, sortX, sortY FROM MDDefinition WHERE (id = @id)
