﻿
CREATE PROCEDURE [dbo].[sproc_countTsColumns]
    @TSid int,
   @ColumnDefinition nvarchar(50),
   @TSColumnCount int OUTPUT
AS
	SET NOCOUNT ON;
SELECT       @TSColumnCount = count(*)
FROM            TSColumns where TSid = @TSid and ColumnDefinition = @ColumnDefinition