﻿
CREATE PROCEDURE [dbo].[sproc_getMDName]
(
	@MDName nvarchar(50),
	@MDIdFound int OUTPUT,
	@MDColumnCount int OUTPUT
)
AS
	
SELECT    @MDIdFound = id 
FROM            MDName
WHERE name = Lower(@MDName);

select @MDColumnCount = count(*) from MDColumns where metadatanameid = @MDIdFound