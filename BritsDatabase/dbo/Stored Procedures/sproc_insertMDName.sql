﻿
CREATE PROCEDURE [dbo].[sproc_insertMDName]
(
	@MDName nvarchar(50),
	@GroupName nvarchar(50),
	@MDid int OUTPUT
)
AS
    Declare @name nvarchar(50)
    
	SET NOCOUNT OFF;
INSERT INTO [MDName] ([name],[groupname]) VALUES (Lower(@MDName),Lower(@GroupName));
	
SELECT @MDid = id,@name = name  FROM MDName WHERE (id = SCOPE_IDENTITY())
