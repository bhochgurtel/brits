﻿
CREATE PROCEDURE [dbo].sproc_updateTsData
(
	@datetimeoccurred datetime,
	@value float,
	@tsid int,
	@mdid int,
	@columnid int,
	@intX int,
	@inty int,
	@Original_datetimeoccurred datetime
)
AS
	SET NOCOUNT OFF;
UPDATE [TSData] SET [datetimeoccurred] = @datetimeoccurred, [value] = @value, [tsid] = @tsid, [mdid] = @mdid, [columnid] = @columnid, [intX] = @intX, [inty] = @inty WHERE (([datetimeoccurred] = @Original_datetimeoccurred));
	
SELECT datetimeoccurred, value, tsid, mdid, columnid, intX, inty FROM TSData WHERE (datetimeoccurred = @datetimeoccurred)
