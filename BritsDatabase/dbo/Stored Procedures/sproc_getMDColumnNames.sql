﻿
CREATE PROCEDURE [dbo].[sproc_getMDColumnNames]
(
   @MDid int,
   @MDCount int OUTPUT
)
AS
	SET NOCOUNT ON;
	
select @MDCount = count(*) from MDColumns where metadatanameid =@MDid	

if @MDCount > 0
   Begin
       SELECT        id, name, type, definition, columnorder, metadatanameid
       FROM            MDColumns where metadatanameid  =@MDid
   End