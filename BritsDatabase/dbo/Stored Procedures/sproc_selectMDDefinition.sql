﻿
CREATE PROCEDURE [dbo].sproc_selectMDDefinition
AS
	SET NOCOUNT ON;
SELECT        id, mdid, columnName, value, sortX, sortY
FROM            MDDefinition
