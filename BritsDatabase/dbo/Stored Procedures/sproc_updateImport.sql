﻿CREATE PROCEDURE [dbo].sproc_updateImport
(
	@ImportDateTime datetime,
	@Description nvarchar(255),
	@Original_ImportId int,
	@ImportId int
)
AS
	SET NOCOUNT OFF;
UPDATE [Import] SET [ImportDateTime] = @ImportDateTime, [Description] = @Description WHERE (([ImportId] = @Original_ImportId));
	
SELECT ImportId, ImportDateTime, Description FROM Import WHERE (ImportId = @ImportId)