﻿CREATE PROCEDURE [dbo].sproc_updateLocation
(
	@LocationType nvarchar(50),
	@LocationUnits nvarchar(50),
	@Latitute nvarchar(50),
	@Longitute nvarchar(50),
	@X nvarchar(50),
	@Y nvarchar(50),
	@Original_ID int,
	@ID int
)
AS
	SET NOCOUNT OFF;
UPDATE [LocationId] SET [LocationType] = @LocationType, [LocationUnits] = @LocationUnits, [Latitute] = @Latitute, [Longitute] = @Longitute, [X] = @X, [Y] = @Y WHERE (([ID] = @Original_ID));
	
SELECT ID, LocationType, LocationUnits, Latitute, Longitute, X, Y FROM LocationId WHERE (ID = @ID)