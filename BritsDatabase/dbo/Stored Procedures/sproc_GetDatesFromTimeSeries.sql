﻿CREATE PROCEDURE [dbo].[sproc_GetDatesFromTimeSeries]
	@TSid int = 0,
	@TimeSeriesName varchar(50)
AS
	SELECT distinct datetimeoccurred from [DisplayTimeSeriesNoPivot2] where tsid = @TSID and TimeSeriesName = @TimeSeriesName order by datetimeoccurred asc
RETURN 0
