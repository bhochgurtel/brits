﻿CREATE PROCEDURE [dbo].sproc_insertImport
(
	@ImportDateTime datetime,
	@Description nvarchar(255)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Import] ([ImportDateTime], [Description]) VALUES (@ImportDateTime, @Description);
	
SELECT ImportId, ImportDateTime, Description FROM Import WHERE (ImportId = SCOPE_IDENTITY())