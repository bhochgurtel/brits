﻿
CREATE PROCEDURE [dbo].[sproc_insertTSName]
(
	@TimeSeriesParentName nvarchar(50),
	@TimeSeriesName nvarchar(50),
	@MetaDataId int,
	@tsId int output
)
AS
	SET NOCOUNT OFF;
INSERT INTO [TSName] ([TimeSeriesParentName],[TimeSeriesName], [MetaDataId]) VALUES (@TimeSeriesParentName,@TimeSeriesName, @MetaDataId);
	
SELECT @tsId=id FROM TSName WHERE (id = SCOPE_IDENTITY())
