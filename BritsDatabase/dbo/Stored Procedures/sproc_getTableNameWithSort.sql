﻿
CREATE PROCEDURE [dbo].[sproc_getTableNameWithSort]
(
   @TableNamePassed nvarchar(50)
)
AS
	SET NOCOUNT OFF;
	SELECT 
       distinct TableName,sortY
       FROM [BRITS].[dbo].[displaymetadatanopivot] 
       where TableName = @TableNamePassed order by sortY