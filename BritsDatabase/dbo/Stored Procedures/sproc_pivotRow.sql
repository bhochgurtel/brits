﻿
CREATE PROCEDURE [dbo].[sproc_pivotRow]
(
   @TableNamePassed nvarchar(50),
   @sortYPassed int 
)
AS
	SET NOCOUNT ON;
	DECLARE @SQLString nvarchar(500);
    DECLARE @ParmDefinition nvarchar(500);

    set @SQLString = N'select * from(select columnname,value from displaymetadatanopivot where tablename=@TableName and sortY=@Sort) as d
Pivot (max(value) for columnname in (AerialFlightLogID,Year,SurveyPeriod,RFObserverID,LRObserverID,PilotID,CompanyID,TransectID,FlightDate,StartTime,EndTime,SecondStartTime,SecondEndTime,FlightOrigin,FlightTransect,FlightDirection,TransectMSL,FlightType,TransectCompleted,WeatherConditions,RFObserver,LRObserver,Pilot,CompanyName,PlaneType,Notes))
as D'

SET @ParmDefinition = N'@TableName nvarchar(50),@Sort int';

execute sp_executesql @SQLString,@ParmDefinition,@TableName = @TableNamePassed,@Sort = @sortYPassed;
	
