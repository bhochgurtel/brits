﻿
CREATE PROCEDURE [dbo].sproc_selectTsColumns
AS
	SET NOCOUNT ON;
SELECT        ID, TSMetaDataId, ColumnName, ColumnType, ColumnFormat, ColumnOrder
FROM            TSColumns
