﻿
CREATE PROCEDURE [dbo].[sproc_insertMDDefinition]
(
	@mdid int,
	@cid int,
	@columnName nvarchar(50),
	@value nvarchar(MAX),
	@sortX int,
	@sortY int,
	@id int OUTPUT
)
AS
	SET NOCOUNT OFF;
INSERT INTO [MDDefinition] ([mdid], [cid],[columnName], [value], [sortX], [sortY]) VALUES (@mdid, @cid, @columnName, @value, @sortX, @sortY);
	
SELECT @id = id FROM MDDefinition WHERE (id = SCOPE_IDENTITY())
