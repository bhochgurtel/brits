﻿
CREATE PROCEDURE [dbo].[sproc_insertTsColumns]
(
    @TSid int,
	@TSMetaDataId int,
	@ColumnName nvarchar(50),
	@ColumnType nvarchar(50),
	@ColumnDefinition nvarchar(50),
	@ColumnFormat nvarchar(50),
	@ColumnOrder int,
	@id int OUTPUT
)
AS
	SET NOCOUNT OFF;
INSERT INTO [TSColumns] ([TSid],[TSMetaDataId], [ColumnName], [ColumnType], [ColumnFormat], [ColumnOrder],[ColumnDefinition]) VALUES (@TSid,@TSMetaDataId, @ColumnName, @ColumnType, @ColumnFormat, @ColumnOrder,@ColumnDefinition);
	
SELECT @id = ID FROM TSColumns WHERE (ID = SCOPE_IDENTITY())
