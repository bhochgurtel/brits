﻿
CREATE PROCEDURE [dbo].[sproc_getTableSort]
(
   @TableNamePassed nvarchar(50)
)
AS
	SET NOCOUNT OFF;
	SELECT 
       distinct sortY
       FROM [BRITS].[dbo].[displaymetadatanopivot] 
       where TableName = @TableNamePassed order by sortY