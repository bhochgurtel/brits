﻿
CREATE PROCEDURE [dbo].[sproc_insertTsTextData]
(
	@DateOccurred datetime,
	@tsid int,
	@columnid int,
	@sortX int,
	@sortY int,
	@Text1 nvarchar(max)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [TSTextData] ([datetimeoccurred], [tsid], [columnid], [intx], [inty], [Text1]) VALUES (@DateOccurred, @tsid, @columnid, @sortX, @sortY, @Text1);

