﻿
CREATE PROCEDURE [dbo].[sproc_selectTsColumnNames]
   @TSid int,
   @ColumnDefinition varchar(50),
   @TSCount int output
AS
	SET NOCOUNT ON;
SELECT        @TSCount = count(*)FROM TSColumns where TSid = @TSid 

if @TSCount > 1
Begin
   SELECT [ID]
      ,[TSid]
      ,[TSMetaDataId]
      ,[ColumnName]
      ,[ColumnType]
      ,[ColumnFormat]
      ,[ColumnOrder]
      ,[ColumnDefinition]
      ,[EngUnits]
      ,[MetricUnits]
      ,[Interval]
      ,[Description]
      ,[DataType]
      ,[engunits_min]
      ,[enguints_max]
      ,[metricunits_min]
      ,[metricunits_max]
    FROM [TSColumns] where TSid = @TSid and ColumnDefinition = @ColumnDefinition
 END