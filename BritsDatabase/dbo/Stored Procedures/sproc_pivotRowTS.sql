﻿
CREATE PROCEDURE [dbo].[sproc_pivotRowTS]
(
   @TableNamePassed nvarchar(50),
   @sortYPassed int,
   @columnNames nvarchar(1000) 
)
AS
	SET NOCOUNT ON;
	DECLARE @SQLString nvarchar(500);
    DECLARE @ParmDefinition nvarchar(500);

    set @SQLString = N'select * from(select columnname,value from DisplayTimeSeriesNoPivot where tablename=@TableName and sortY=@Sort) as d
Pivot (max(value) for columnname in (@columns) )
as D'

SET @ParmDefinition = N'@TableName nvarchar(50),@Sort int,@columns nvarchar(200)';

execute sp_executesql @SQLString,@ParmDefinition,@TableName = @TableNamePassed,@Sort = @sortYPassed,@columns=@columnNames;
	
