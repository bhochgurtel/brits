﻿
CREATE PROCEDURE [dbo].[sproc_selectTSName]
   @TimeSeriesName nvarchar(50),
   @TimeSeriesId int OUTPUT,
   @MetaDataId int OUTPUT
AS
SET NOCOUNT ON;
SELECT    @TimeSeriesId = id,@MetaDataId = MetaDataId
FROM      TSName
WHERE TimeSeriesName = @TimeSeriesName 
