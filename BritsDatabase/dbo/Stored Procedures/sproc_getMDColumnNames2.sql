﻿
CREATE PROCEDURE [dbo].[sproc_getMDColumnNames2]
(
   @MDName nvarchar(50),
   @MDCount int OUTPUT
)
AS
	SET NOCOUNT ON;
	
select @MDCount = count(*) from MDColumns where definition =@MDName	

if @MDCount > 0
   Begin
       SELECT        id, name, type, definition, columnorder, metadatanameid
       FROM            MDColumns where name =@MDName
   End