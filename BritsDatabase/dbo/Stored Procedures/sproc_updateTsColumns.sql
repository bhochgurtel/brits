﻿
CREATE PROCEDURE [dbo].sproc_updateTsColumns
(
	@TSMetaDataId int,
	@ColumnName nvarchar(50),
	@ColumnType nvarchar(50),
	@ColumnFormat nvarchar(50),
	@ColumnOrder int,
	@Original_ID int,
	@ID int
)
AS
	SET NOCOUNT OFF;
UPDATE [TSColumns] SET [TSMetaDataId] = @TSMetaDataId, [ColumnName] = @ColumnName, [ColumnType] = @ColumnType, [ColumnFormat] = @ColumnFormat, [ColumnOrder] = @ColumnOrder WHERE (([ID] = @Original_ID));
	
SELECT ID, TSMetaDataId, ColumnName, ColumnType, ColumnFormat, ColumnOrder FROM TSColumns WHERE (ID = @ID)
