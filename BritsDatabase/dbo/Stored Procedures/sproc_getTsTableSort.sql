﻿
CREATE PROCEDURE [dbo].[sproc_getTsTableSort]
(
   @TableNamePassed nvarchar(50)
)
AS
	SET NOCOUNT OFF;
	SELECT 
       distinct intY
       FROM [BRITS].[dbo].[displaytimeseriesnopivot] 
       where TimeSeriesName = @TableNamePassed order by intY