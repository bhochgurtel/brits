﻿
CREATE PROCEDURE [dbo].sproc_updateTSName
(
	@TimeSeriesName nvarchar(50),
	@MetaDataId int,
	@Original_id int,
	@id int
)
AS
	SET NOCOUNT OFF;
UPDATE [TSName] SET [TimeSeriesName] = @TimeSeriesName, [MetaDataId] = @MetaDataId WHERE (([id] = @Original_id));
	
SELECT id, TimeSeriesName, MetaDataId FROM TSName WHERE (id = @id)
