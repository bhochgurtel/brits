﻿CREATE PROCEDURE [dbo].[sproc_getTsColumns]
(
   @TSID int,
   @ColumnDefinition varchar(50)
)
AS
	SET NOCOUNT OFF;
	SELECT  distinct [ColumnName]
      ,[intX]
      ,[TSid]
  FROM [DisplayTimeSeriesNoPivot2]
  where TSID = @TSID and ColumnDefinition = @ColumnDefinition order by intX
