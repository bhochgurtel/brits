﻿CREATE PROCEDURE sproc_insertMdTsBridge
	-- Add the parameters for the stored procedure here
	@metaid int,
	@tsid int,
	@TsMdBridge int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    INSERT INTO [MDTSBridge] ([metaid],[tsid]) VALUES (@metaid,@tsid)
	
    SELECT @TsMdBridge = id FROM  [MDTSBridge] WHERE (id = SCOPE_IDENTITY())
    
END
