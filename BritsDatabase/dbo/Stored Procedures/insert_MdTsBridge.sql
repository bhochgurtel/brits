﻿
CREATE PROCEDURE [dbo].[insert_MdTsBridge]
(
	@metaid int,
	@tsid int,
	@TsMdBridge int OUTPUT
)
AS
	SET NOCOUNT OFF;
INSERT INTO [MDTSBridge] ([metaid], [tsid]) VALUES (@metaid, @tsid);
	
SELECT @TsMdBridge = id FROM MDTSBridge WHERE (id = SCOPE_IDENTITY())
