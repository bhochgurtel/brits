﻿
CREATE PROCEDURE [dbo].sproc_updateMDColumnNames
(
	@name nvarchar(50),
	@type nvarchar(50),
	@definition nvarchar(50),
	@columnorder int,
	@metadatanameid int,
	@Original_id int,
	@id int
)
AS
	SET NOCOUNT OFF;
UPDATE [MDColumns] SET [name] = @name, [type] = @type, [definition] = @definition, [columnorder] = @columnorder, [metadatanameid] = @metadatanameid WHERE (([id] = @Original_id));
	
SELECT id, name, type, definition, columnorder, metadatanameid FROM MDColumns WHERE (id = @id)
