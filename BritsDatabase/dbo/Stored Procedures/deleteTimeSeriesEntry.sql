﻿CREATE PROCEDURE [dbo].[deleteTimeSeriesEntry]
	@tsId int = 0,
	@dateTimeOccurred datetime
AS
	delete from TSData where tsid = @tsId and datetimeoccurred = @dateTimeOccurred
RETURN 0
