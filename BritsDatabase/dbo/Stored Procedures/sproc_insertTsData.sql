﻿
CREATE PROCEDURE [dbo].[sproc_insertTsData]
(
	@datetimeoccurred datetime,
	@value float,
	@tsid int,
	@mdid int,
	@columnid int,
	@sortX int,
	@sortY int
)
AS
	SET NOCOUNT OFF;
INSERT INTO [TSData] ([datetimeoccurred], [value], [tsid], [mdid], [columnid], [intX], [inty]) VALUES (@datetimeoccurred, @value, @tsid, @mdid, @columnid, @sortX, @sortY);
	
SELECT datetimeoccurred, value, tsid, mdid, columnid, intX, inty FROM TSData WHERE (datetimeoccurred = @datetimeoccurred)
