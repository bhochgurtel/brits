﻿
CREATE PROCEDURE [dbo].sproc_updateMDName
(
	@Expr2 nvarchar(50),
	@Original_Expr1 int,
	@id int
)
AS
	SET NOCOUNT OFF;
UPDATE [MDName] SET [name] = @Expr2 WHERE (([id] = @Original_Expr1));
	
SELECT id AS Expr1, name AS Expr2 FROM MDName WHERE (id = @id)
