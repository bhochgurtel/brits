﻿CREATE TABLE [dbo].[MetaDataColumns] (
    [id]                  INT            IDENTITY (1, 1) NOT NULL,
    [metaDataNameId]      INT            NOT NULL,
    [name]                NVARCHAR (255) NOT NULL,
    [type]                NVARCHAR (20)  NOT NULL,
    [definition]          NVARCHAR (500) NULL,
    [originalColumnOrder] INT            NOT NULL,
    [importid]            INT            NOT NULL,
    CONSTRAINT [PK_MetaData_1] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_MetaDataColumns_MetaDataNames] FOREIGN KEY ([metaDataNameId]) REFERENCES [dbo].[MetaDataNames] ([id]) ON DELETE CASCADE ON UPDATE CASCADE
);

