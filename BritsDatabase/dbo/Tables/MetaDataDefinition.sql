﻿CREATE TABLE [dbo].[MetaDataDefinition] (
    [id]       INT            NOT NULL,
    [columnId] INT            NULL,
    [value]    NVARCHAR (500) NULL,
    [sort]     INT            NULL,
    CONSTRAINT [PK_MetaDataDefinition] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_MetaDataDefinition_MetaDataColumns] FOREIGN KEY ([columnId]) REFERENCES [dbo].[MetaDataColumns] ([id]) ON DELETE CASCADE ON UPDATE CASCADE
);

