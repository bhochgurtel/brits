﻿CREATE TABLE [dbo].[LocationId] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [LocationType]  NVARCHAR (50) NULL,
    [LocationUnits] NVARCHAR (50) NULL,
    [Latitute]      NVARCHAR (50) NULL,
    [Longitute]     NVARCHAR (50) NULL,
    [X]             NVARCHAR (50) NULL,
    [Y]             NVARCHAR (50) NULL,
    CONSTRAINT [PK_LocationId] PRIMARY KEY CLUSTERED ([ID] ASC)
);

