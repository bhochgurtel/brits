﻿CREATE TABLE [dbo].[MDDefinition] (
    [id]         INT            IDENTITY (1, 1) NOT NULL,
    [mdid]       INT            NULL,
    [cid]        INT            NULL,
    [columnName] NVARCHAR (50)  NULL,
    [value]      NVARCHAR (MAX) NULL,
    [sortX]      INT            NULL,
    [sortY]      INT            NULL,
    CONSTRAINT [PK_MDDefinition] PRIMARY KEY CLUSTERED ([id] ASC)
);

