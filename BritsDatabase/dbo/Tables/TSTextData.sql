﻿CREATE TABLE [dbo].[TSTextData] (
    [ID]               INT             IDENTITY (1, 1) NOT NULL,
    [datetimeoccurred] DATETIME2 (7)   NULL,
    [tsid]             INT             NULL,
    [columnid]         INT             NULL,
    [intX]             INT             NULL,
    [intY]             INT             NULL,
    [Text1]            NVARCHAR (4000) NULL,
    CONSTRAINT [PK_TSTextDAta] PRIMARY KEY CLUSTERED ([ID] ASC)
);

