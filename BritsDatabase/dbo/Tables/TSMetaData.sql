﻿CREATE TABLE [dbo].[TSMetaData] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [TSViewName]     NVARCHAR (255) NULL,
    [MDViewName]     NVARCHAR (255) NULL,
    [LocationId]     INT            NULL,
    [TimeSeriesId]   INT            NULL,
    [TimeSeriesName] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_TSMetaData] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TSMetaData_LocationId] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[LocationId] ([ID])
);

