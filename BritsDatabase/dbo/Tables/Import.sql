﻿CREATE TABLE [dbo].[Import] (
    [ImportId]       INT            IDENTITY (1, 1) NOT NULL,
    [ImportDateTime] DATETIME       NULL,
    [Description]    NVARCHAR (255) NULL,
    CONSTRAINT [PK_Import] PRIMARY KEY CLUSTERED ([ImportId] ASC)
);

