﻿CREATE TABLE [dbo].[TSData] (
    [id]               INT        IDENTITY (1, 1) NOT NULL,
    [datetimeoccurred] DATETIME   NOT NULL,
    [value]            FLOAT (53) NOT NULL,
    [tsid]             INT        NOT NULL,
    [mdid]             INT        NOT NULL,
    [columnid]         INT        NOT NULL,
    [intX]             INT        NOT NULL,
    [intY]             INT        NOT NULL
);


GO
CREATE CLUSTERED INDEX [PK_Clustered_TSDATA_id]
    ON [dbo].[TSData]([id] ASC);

