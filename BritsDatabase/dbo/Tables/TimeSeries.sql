﻿CREATE TABLE [dbo].[TimeSeries] (
    [ID]               INT      IDENTITY (1, 1) NOT NULL,
    [DateTimeOccurred] DATETIME NULL,
    [DataId]           INT      NULL,
    [LocationId]       INT      NULL,
    [MetaDataId]       INT      NULL,
    [ImportId]         INT      NULL,
    [TextDataId]       INT      NULL,
    CONSTRAINT [PK_TimeSeries] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TimeSeries_Import] FOREIGN KEY ([ImportId]) REFERENCES [dbo].[Import] ([ImportId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TimeSeries_LocationId] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[LocationId] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TimeSeries_MetaData] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[MetaDataOld] ([ID]),
    CONSTRAINT [FK_TimeSeries_TimeSeriesData] FOREIGN KEY ([DataId]) REFERENCES [dbo].[TimeSeriesData] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
);

