﻿CREATE TABLE [dbo].[TSColumns] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [TSid]             INT           NOT NULL,
    [TSMetaDataId]     INT           NOT NULL,
    [ColumnName]       NVARCHAR (50) NULL,
    [ColumnType]       NVARCHAR (50) NULL,
    [ColumnFormat]     NVARCHAR (50) NULL,
    [ColumnOrder]      INT           NULL,
    [ColumnDefinition] NVARCHAR (50) NULL,
    [EngUnits]         NVARCHAR (50) NULL,
    [MetricUnits]      NVARCHAR (50) NULL,
    [Interval]         NVARCHAR (50) NULL,
    [Description]      NVARCHAR (50) NULL,
    [DataType]         NVARCHAR (50) NULL,
    [engunits_min]     FLOAT (53)    NULL,
    [enguints_max]     FLOAT (53)    NULL,
    [metricunits_min]  FLOAT (53)    NULL,
    [metricunits_max]  FLOAT (53)    NULL,
    CONSTRAINT [PK_TSColumns] PRIMARY KEY CLUSTERED ([ID] ASC)
);

