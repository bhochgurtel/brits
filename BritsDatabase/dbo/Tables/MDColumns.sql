﻿CREATE TABLE [dbo].[MDColumns] (
    [id]             INT           IDENTITY (1, 1) NOT NULL,
    [name]           NVARCHAR (50) NULL,
    [type]           NVARCHAR (50) NULL,
    [definition]     NVARCHAR (50) NULL,
    [columnorder]    INT           NULL,
    [metadatanameid] INT           NULL,
    CONSTRAINT [PK_MDColumns] PRIMARY KEY CLUSTERED ([id] ASC)
);

