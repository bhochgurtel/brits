﻿CREATE TABLE [dbo].[TimeSeriesData] (
    [ID]                    INT        IDENTITY (1, 1) NOT NULL,
    [TimeSeriesMeataDataId] INT        NULL,
    [TextDataId]            INT        NULL,
    [ImportId]              INT        NULL,
    [TSFloat1]              FLOAT (53) NULL,
    [TSFloat2]              FLOAT (53) NULL,
    [TSFloat3]              FLOAT (53) NULL,
    [TSFloat4]              FLOAT (53) NULL,
    [TSFloat5]              FLOAT (53) NULL,
    [TSFloat6]              FLOAT (53) NULL,
    [TSFloat7]              FLOAT (53) NULL,
    [TSFloat8]              FLOAT (53) NULL,
    [TSFloat9]              FLOAT (53) NULL,
    [TSFloat10]             FLOAT (53) NULL,
    CONSTRAINT [PK_TimeSeriesData] PRIMARY KEY CLUSTERED ([ID] ASC)
);

