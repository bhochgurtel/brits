﻿CREATE TABLE [dbo].[MDName] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [name] NVARCHAR (50) NULL,
    [groupname] NCHAR(50) NULL, 
    CONSTRAINT [PK_MDName] PRIMARY KEY CLUSTERED ([id] ASC)
);

