﻿CREATE TABLE [dbo].[MDTSBridge] (
    [id]     INT IDENTITY (1, 1) NOT NULL,
    [metaid] INT NULL,
    [tsid]   INT NULL,
    CONSTRAINT [PK_MDTSBridge] PRIMARY KEY CLUSTERED ([id] ASC)
);

