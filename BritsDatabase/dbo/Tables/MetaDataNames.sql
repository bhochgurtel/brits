﻿CREATE TABLE [dbo].[MetaDataNames] (
    [id]         INT            IDENTITY (1, 1) NOT NULL,
    [name]       NVARCHAR (255) NOT NULL,
    [definition] NVARCHAR (500) NULL,
    CONSTRAINT [PK_MetaDataNames] PRIMARY KEY CLUSTERED ([id] ASC)
);

