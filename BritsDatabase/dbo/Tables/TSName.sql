﻿CREATE TABLE [dbo].[TSName] (
    [id]                   INT               IDENTITY (1, 1) NOT NULL,
    [TimeSeriesParentName] NVARCHAR (50)     NOT NULL,
    [TimeSeriesName]       NVARCHAR (50)     NULL,
    [MetaDataId]           INT               NOT NULL,
    [Location]             [sys].[geography] NULL,
    CONSTRAINT [PK_TSName] PRIMARY KEY CLUSTERED ([id] ASC)
);

