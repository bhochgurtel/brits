﻿CREATE TABLE [dbo].[MdPages] (
    [id]   INT            NOT NULL,
    [mdid] INT            NULL,
    [Name] NVARCHAR (255) NULL,
    CONSTRAINT [PK_MdPages] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MdPages]
    ON [dbo].[MdPages]([id] ASC);

