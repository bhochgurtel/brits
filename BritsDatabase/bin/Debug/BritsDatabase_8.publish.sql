﻿/*
Deployment script for BritsDatabase

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "BritsDatabase"
:setvar DefaultFilePrefix "BritsDatabase"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL10_50.BDH\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL10_50.BDH\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Dropping [dbo].[vw_showMetaDataTablesWithSort].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_showMetaDataTablesWithSort';


GO
PRINT N'Dropping [dbo].[vw_showMetaDataTablesWithSort].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_showMetaDataTablesWithSort';


GO
PRINT N'Altering [dbo].[vw_showMetaDataTablesWithSort]...';


GO
/****** Script for SelectTopNRows command from SSMS  ******/
ALTER view vw_showMetaDataTablesWithSort as
SELECT 
      *
       FROM [dbo].[DisplayMetaDataNoPivot]
GO
PRINT N'Creating [dbo].[View1]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE VIEW [dbo].[View1]
	AS SELECT * FROM [MDDefinition] where columnName not like 'Column%'
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [dbo].[sproc_getMetaDataColumns]...';


GO

CREATE PROCEDURE [dbo].[sproc_getMetaDataColumns]
(
   @MDID int
)
AS
	SET NOCOUNT OFF;
	SELECT  distinct [columnName]
      ,[sortX]
	  ,[sortY]
      ,[id]
  FROM [vw_showMetaDataTablesWithSort]
  where id = @MDID order by sorty,sortX
GO
PRINT N'Update complete.';


GO
