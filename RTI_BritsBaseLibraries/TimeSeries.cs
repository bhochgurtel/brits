﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using rti.common.cs.db.SqlServer;

namespace RTI_BritsBaseLibraries
{
    public class TimeSeries : Base 
    {
        public TimeSeries()
        {
        }

        public TimeSeries(string connectionString)
        {
            _connectionString = connectionString;
        }

         /// <summary>
        /// Constructor to tell the file type and path
        /// </summary>
        /// <param name="type">Excel or CSV</param>
        /// <param name="path">path to the file</param>
        public TimeSeries(string connectionString, string metaDataName, string tsParentName,string tsName, DataTable tsFromExcel)
        {
            _tsExcel = tsFromExcel;
            _connectionString = connectionString;
            _metaDataName = metaDataName;
            _tsName = tsName;
            _tsParentName = tsParentName;
            Process();
        }

        public TimeSeries(string connectionString, string metaDataName, string tsParentName)
        {
            _connectionString = connectionString;
            _metaDataName = metaDataName;
            //_tsName = tsName;
            _tsParentName = tsParentName;
        }

        public TimeSeries(string connectionString, string metaDataName, string tsParentName,string tsName)
        {
            _connectionString = connectionString;
            _metaDataName = metaDataName;
            _tsName = tsName;
            _tsParentName = tsParentName;
        }

        public virtual void ProcessForLoad()
        {
            DoesMdExist();
            DoesTsExist();

            if (!this._tsExists)
            {
                throw new Exception("The Time Series You have asked for does not exist in the db");
            }

            AreTsColumnsDefined();

            if (this._tsDefined)
                GetTsColumnNames();
            else
                throw new Exception("The Time Series You have asked for does not have its columns define in the db. Please reload.");

        }

        public virtual void Process()
        {
            DoesMdExist();
            DoesTsExist();

            if (!this._tsExists)
            {
                this.CreateTs();
                this.CreateTsMdBridge();
            }

            AreTsColumnsDefined();

            if (this._tsDefined)
                GetTsColumnNames();
            else
                CreateTsColumnNames();

        }

       
        protected virtual void CreateTs()
        {
            if (_tsName == string.Empty)
                throw new Exception("Must provide a time series name");

            SqlParameter[] myParams = new SqlParameter[4];
            myParams[0] = new SqlParameter("@TimeSeriesParentName", SqlDbType.NVarChar);
            myParams[0].Value = _tsParentName.ToLower();
            myParams[1] = new SqlParameter("@TimeSeriesName", SqlDbType.NVarChar);
            myParams[1].Value = _tsName.ToLower();
            myParams[2] = new SqlParameter("@MetaDataId", SqlDbType.Int);
            myParams[2].Value = _metaDataId;
            myParams[3] = new SqlParameter("@tsId", SqlDbType.Int);
            myParams[3].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertTSName", myParams);
            this._tsId = Int32.Parse(myParams[2].Value.ToString());
        }

        protected virtual DataTable CreateBulkDataTable()
        {
            DataTable bulkData = new DataTable();
            bulkData.Clear();
            bulkData.Columns.Add("datetimeoccurred", typeof(DateTime));
            bulkData.Columns.Add("value", typeof(float));
            bulkData.Columns.Add("tsid", typeof(int));
            bulkData.Columns.Add("mdid", typeof(int));
            bulkData.Columns.Add("columnid", typeof(int));
            bulkData.Columns.Add("intX", typeof(int));
            bulkData.Columns.Add("intY", typeof(int));
            return bulkData;
        }

        protected virtual DataTable CreateBulkTextTable()
        {
            DataTable bulkData = new DataTable();
            bulkData.Clear();
            bulkData.Columns.Add("datetimeoccurred", typeof(DateTime));
            // bulkData.Columns.Add("value", typeof(float));
            bulkData.Columns.Add("tsid", typeof(int));
            bulkData.Columns.Add("columnid", typeof(int));
            bulkData.Columns.Add("intX", typeof(int));
            bulkData.Columns.Add("intY", typeof(int));
            bulkData.Columns.Add("Text1", typeof(String));
            return bulkData;
        }

        public virtual string PopulateBulkDataTable()
        {
            DataTable bulkValues = CreateBulkDataTable();
            DataTable bulkTextValues = CreateBulkTextTable();
            //Load the current dates for the time series so we can check for duplicates.
            //Too slow to use constraint in database
            GetCurrentDatesForTimeSeries();
            StringBuilder errors = new StringBuilder();



            int x = 0;
            int y = 1;
            Hashtable ht = new Hashtable();
            foreach (DataRow dr in _tsExcel.Rows)
            {
                x = 0;
                bool duplicate = testForDuplicate(dr[0].ToString());

                foreach (DataColumn dc in _tsExcel.Columns)
                {
                    //bool IsNumeric = testIfNumeric(dr[dc.ColumnName].ToString());
                    //ht = (Hashtable)_htTable[_tsName];
                    Type myType = dr[dc.ColumnName].GetType();
                    column myColumn = (column)_htTable[dc.ColumnName.Trim()];

                    if (IsNumeric(myType))
                    {
                        if (x != 0)
                        {
                            if (_deleteDuplicates || !duplicate)
                            {
                                try
                                {
                                    bulkValues.Rows.Add(Convert.ToDateTime(dr[0]), decimal.Parse(dr[dc.ColumnName].ToString()), _tsId, _metaDataId, myColumn.columnId, x, y);
                                }
                                catch (Exception e)
                                {
                                    errors.Append(e.ToString().Substring(0,210) + " " + dr[0]);
                                }
                            }
                        }
                        else if (dr[dc.ColumnName].ToString().Length > 0)
                        {
                            try
                            {
                                bulkTextValues.Rows.Add(Convert.ToDateTime(dr[0]), _tsId, myColumn.columnId, x, y, dr[dc.ColumnName].ToString());
                            } catch (Exception e)
                            {
                                errors.Append(e.ToString().Substring(0,210) + " " + dr[0]);
                            }
                        }
                    }

                    x++;
                }
                y++;

                //since we've already added it, if we add it again it would be a duplicate.
                //a little preventive medicine
                _duplicateHash[dr[0].ToString()] = _tsId;


            }

            if (bulkValues.Rows.Count > 0)
                SendTsDataBulk(bulkValues, "dbo.TSData");

            if (bulkTextValues.Rows.Count > 0)
                SendTsDataBulk(bulkTextValues, "dbo.TSTextData");
           
            return errors.ToString();
        }

        protected virtual bool testForDuplicate(string p)
        {

            if (_duplicateHash.Keys.Count == 0 || !_duplicateHash.ContainsKey(p))
            {
                //_duplicateHash[p] = tsId;
                return false;
            }
            else
            {
                if (_deleteDuplicates)
                    this.deleteDuplicateEntry(DateTime.Parse(p));

                //_duplicateList.Append(p + ",");

                return true;
            }
        }

        public virtual void SendTsData()
        {
            int x = 0;
            int y = 1;
            Hashtable ht = new Hashtable();
            foreach (DataRow dr in _tsExcel.Rows)
            {
                x = 0;

                foreach (DataColumn dc in _tsExcel.Columns)
                {
                    ht = (Hashtable)_htTable[_tsName];
                    column myColumn = (column)ht[dc.ColumnName];
                    //skip the date column because we'll record it will all the data

                    if (x != 0)
                    {
                        try
                        {
                            SqlParameter[] myParams = new SqlParameter[7];
                            myParams[0] = new SqlParameter("@datetimeoccurred", SqlDbType.DateTime);
                            myParams[0].Value = dr[0].ToString();
                            myParams[1] = new SqlParameter("@value", SqlDbType.Float);
                            myParams[1].Value = Single.Parse(dr[dc.ColumnName].ToString());
                            myParams[2] = new SqlParameter("@tsid", SqlDbType.Int);
                            myParams[2].Value = _tsId;
                            myParams[3] = new SqlParameter("@mdid", SqlDbType.Int);
                            myParams[3].Value = _metaDataId;
                            myParams[4] = new SqlParameter("@columnId", SqlDbType.Int);
                            myParams[4].Value = myColumn.columnId;
                            myParams[5] = new SqlParameter("@sortX", SqlDbType.Int);
                            myParams[5].Value = x;
                            myParams[6] = new SqlParameter("@sortY", SqlDbType.Int);
                            myParams[6].Value = y;

                            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertTsData", myParams);
                            myColumn.columnId = Int32.Parse(myParams[5].Value.ToString());
                        }
                        catch (Exception e)
                        {
                            //if other called failed, it may be a comment.  Let's make it one.
                            if (dr[dc.ColumnName].ToString() != string.Empty)
                            {
                                SqlParameter[] myParams = new SqlParameter[6];
                                myParams[0] = new SqlParameter("@DateOccurred", SqlDbType.DateTime);
                                myParams[0].Value = dr[0].ToString();
                                myParams[1] = new SqlParameter("@tsid", SqlDbType.Int);
                                myParams[1].Value = _tsId;
                                myParams[2] = new SqlParameter("@columnId", SqlDbType.Int);
                                myParams[2].Value = myColumn.columnId;
                                myParams[3] = new SqlParameter("@sortX", SqlDbType.Int);
                                myParams[3].Value = x;
                                myParams[4] = new SqlParameter("@sortY", SqlDbType.Int);
                                myParams[4].Value = y;
                                myParams[5] = new SqlParameter("@Text1", SqlDbType.NVarChar, 4000);
                                myParams[5].Value = dr[dc.ColumnName].ToString();
                                SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertTsTextData", myParams);
                                //myColumn.columnId = Int32.Parse(myParams[5].Value.ToString());
                            }
                        }

                    }
                    //else if (x!=0)
                    //{
                    //    dateAndTimeCombined = dr[0].ToString() + " " + dr[dc.ColumnName].ToString();

                    //}
                    x++;
                }

                y++;
            }

        }

        public virtual void SendTsDataBulk(DataTable localDt, string tableName)
        {
            SqlConnection myConnection = new SqlConnection(_connectionString);
            myConnection.Open();

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(myConnection))
            {
                bulkCopy.DestinationTableName = tableName;//"dbo.TSData";

                foreach (DataColumn column in localDt.Columns)
                    bulkCopy.ColumnMappings.Add(column.ColumnName, column.ColumnName);

                bulkCopy.BulkCopyTimeout = 300;

                try
                {
                    // Write from the source to the destination
                    bulkCopy.WriteToServer(localDt);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    // Close the SqlDataReader. The SqlBulkCopy 
                    // object is automatically closed at the end 
                    // of the using block.
                    myConnection.Close();
                }
            }

            myConnection.Dispose();
        }

        protected virtual void deleteDuplicateEntry(DateTime dateTimeOccurred)
        {
            SqlParameter[] myParams = new SqlParameter[2];
            myParams[0] = new SqlParameter("@dateTimeOccurred", SqlDbType.Int);
            myParams[0].Value = _metaDataId;
            myParams[1] = new SqlParameter("@tsid", SqlDbType.Int);
            myParams[1].Value = this._tsId;
            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_deleteTimeSeriesEntry", myParams);
        }

        //this needs to be select distinct
        protected virtual void GetCurrentDatesForTimeSeries()
        {
            SqlParameter[] myParams = new SqlParameter[2];
            myParams[0] = new SqlParameter("@TSid", SqlDbType.Int);
            myParams[0].Value = _tsId;
            myParams[1] = new SqlParameter("@TimeSeriesName", SqlDbType.VarChar);
            myParams[1].Value = _tsName;
            SqlDataReader rdr = SqlHelper.ExecuteReader(_connectionString, CommandType.StoredProcedure, "sproc_GetDatesFromTimeSeries", myParams);

            while (rdr.Read())
            {
                this._duplicateHash[rdr[0].ToString()] = _tsId;
            }

            rdr.Close();
        }

        //http://michaeljswart.com/2011/06/forget-about-pivot/
        //make sure that intX and intY ARE not in the datatable passed in.  They will prevent the time series data for a particular
        //date and time from being on the same row.  It's way ugly so just skip it. 
        protected virtual DataTable Pivot(DataTable dt, string pivotColumn, string pivotValue, string tableName)
        {
            // find primary key columns 
            //(i.e. everything but pivot column and pivot value)
            DataTable temp = dt.Copy();
            temp.TableName = tableName;
            temp.Columns.Remove(pivotColumn);
            temp.Columns.Remove(pivotValue);
            string[] pkColumnNames = temp.Columns.Cast<DataColumn>()
                .Select(c => c.ColumnName)
                .ToArray();

            // prep results table
            DataTable result = temp.DefaultView.ToTable(true, pkColumnNames).Copy();
            result.PrimaryKey = result.Columns.Cast<DataColumn>().ToArray();
            dt.AsEnumerable()
                .Select(r => r[pivotColumn].ToString())
                .Distinct().ToList()
                .ForEach(c => result.Columns.Add(c, Type.GetType("System.String")));

            // load it
            foreach (DataRow row in dt.Rows)
            {
                // find row to update
                DataRow aggRow = result.Rows.Find(
                    pkColumnNames
                        .Select(c => row[c])
                        .ToArray());
                // the aggregate used here is LATEST 
                // adjust the next line if you want (SUM, MAX, etc...)
                aggRow[row[pivotColumn].ToString()] = row[pivotValue];
            }

            return result;
        }

        public virtual void CreateViewForTimeSeries()
        {
            if (this._viewCode == string.Empty)
                getTsColumnsForView();

            generateNewView("_timeseries_" +_tsName);
        }

    }
}
