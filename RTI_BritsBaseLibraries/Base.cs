﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using rti.common.cs.db.SqlServer;

namespace RTI_BritsBaseLibraries
{
    public abstract class Base
    {
        public enum excelFileType { xlsx, csv };
        protected string _path = string.Empty;
        protected string _metaDataName = string.Empty;
        protected int _metaDataId = -1;
        protected excelFileType _fileType;
        protected string _connectionString = string.Empty;
        protected string _tsName;
        protected string _tsParentName;
        protected int _tsId;
        protected Hashtable _tsList = new Hashtable();
        protected int _TsMdBridge;
        protected DataTable _tsExcel;
        protected Hashtable _htTable = new Hashtable();
        protected Hashtable _duplicateHash = new Hashtable();
        protected bool _tsExists = false;
        protected bool _tsDefined = false;
        protected bool _tsColumnsExists = false;
        protected string _viewCode = string.Empty;
        protected bool _deleteDuplicates = false;
        protected StringBuilder _duplicateList = new StringBuilder();
        protected bool _metaDataExist = false;
        //protected DataSet _excelData;

        /// <summary>
        /// Struct used in hashtables to store information about a column
        /// </summary>
        public struct column
        {
            public int tsId;
            public int columnId;
            public int columnOrder;
            public int metaDataId;
            public string type;
            public string definition;
        }


        protected excelFileType FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        public string Path
        {
            get
            {
                return _path;
            }
            set
            {
                _path = value;
            }
        }

        public string DuplicateList
        {
            get
            {
                return _duplicateList.ToString();
            }
        }

        public int tsId
        {
            get { return _tsId; }
            set { _tsId = value; }
        }

        public string MdName
        {
            get { return _tsName; }
            set { _tsName = value; }
        }

        public string MetaDataName
        {
            get { return _metaDataName; }
            set { _metaDataName = value; }
        }

        public int MetaDataId
        {
            get { return _metaDataId; }
            set { _metaDataId = value; }
        }

        public bool DeleteDuplicates
        {
            get { return _deleteDuplicates; }
            set { _deleteDuplicates = value; }
        }

       


       

        protected void AreTsColumnsDefined()
        {
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@TSid", SqlDbType.Int);
            myParams[0].Value = this._tsId;
            myParams[1] = new SqlParameter("@ColumnDefinition", SqlDbType.VarChar);
            myParams[1].Value = this._tsName;
            myParams[2] = new SqlParameter("@TSColumnCount", SqlDbType.Int);
            myParams[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_countTsColumns", myParams);
            int count = 0;

            if (myParams[1].Value.ToString() != string.Empty)
                count = Int32.Parse(myParams[2].Value.ToString());

            if (count > 0)
                _tsDefined = true;
        }
        public void DoesMdExist()
        {

            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@MDName", SqlDbType.NVarChar);
            myParams[0].Value = _metaDataName;
            myParams[1] = new SqlParameter("@MDIdFound", SqlDbType.Int);
            myParams[1].Direction = ParameterDirection.Output;
            myParams[2] = new SqlParameter("@MDColumnCount", SqlDbType.Int);
            myParams[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_getMDName", myParams);


            if (myParams[1].Value.ToString() != string.Empty)
                _metaDataId = Int32.Parse(myParams[1].Value.ToString());

            if (_metaDataId == -1)
                _metaDataExist = false;
            else
                _metaDataExist = true;
        }


        protected void DoesTsExist()
        {
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@TimeSeriesName", SqlDbType.NVarChar, 50);
            myParams[0].Value = _tsName;
            myParams[1] = new SqlParameter("@TimeSeriesId", SqlDbType.Int);
            myParams[1].Direction = ParameterDirection.Output;
            myParams[2] = new SqlParameter("@MetaDataId", SqlDbType.Int);
            myParams[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_selectTSName", myParams);
            int tempId = 0;
            int metaDataId = 0;

            if (myParams[1].Value.ToString() != string.Empty)
                tempId = Int32.Parse(myParams[1].Value.ToString());

            if (myParams[2].Value.ToString() != string.Empty)
                metaDataId = Int32.Parse(myParams[2].Value.ToString());

            if (tempId > 0)
            {
                this._tsExists = true;
                _tsId = tempId;
            }

            if (metaDataId > 0)
                _metaDataId = metaDataId;
        }


        

        protected void CreateTsMdBridge()
        {
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@metaid", SqlDbType.Int);
            myParams[0].Value = _metaDataId;
            myParams[1] = new SqlParameter("@tsid", SqlDbType.Int);
            myParams[1].Value = this._tsId;
            myParams[2] = new SqlParameter("@TsMdBridge", SqlDbType.Int);
            myParams[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertMdTsBridge", myParams);
            this._TsMdBridge = Int32.Parse(myParams[1].Value.ToString());
        }

        public void CreateTsColumnNames()
        {
            if (this._tsExcel.Rows.Count == 0)
                throw new Exception("No data found in Excel file");

            int columnOrder = 0;
            Hashtable ht = new Hashtable();

            foreach (DataColumn dc in this._tsExcel.Columns)
            {
                column myColumn = new column();
                myColumn.type = dc.DataType.ToString();
                myColumn.definition = dc.ColumnName;
                myColumn.tsId = _tsId;
                myColumn.columnOrder = columnOrder;


                SqlParameter[] myParams = new SqlParameter[8];
                myParams[0] = new SqlParameter("@TSMetaDataId", SqlDbType.Int);
                myParams[0].Value = _metaDataId;
                myParams[1] = new SqlParameter("@ColumnName", SqlDbType.NVarChar, 50);
                myParams[1].Value = dc.ColumnName.ToString();
                myParams[2] = new SqlParameter("@ColumnType", SqlDbType.NVarChar, 50);
                myParams[2].Value = dc.DataType.ToString();
                myParams[3] = new SqlParameter("@ColumnDefinition", SqlDbType.NVarChar, 50);
                myParams[3].Value = this._tsName;
                myParams[4] = new SqlParameter("@ColumnOrder", SqlDbType.Int);
                myParams[4].Value = columnOrder;
                myParams[5] = new SqlParameter("@ColumnFormat", SqlDbType.NVarChar, 50);
                //this may need to be user supplied
                myParams[5].Value = "soon to be filled";
                myParams[6] = new SqlParameter("@TSid", SqlDbType.Int);
                myParams[6].Value = _tsId;
                myParams[7] = new SqlParameter("@id", SqlDbType.Int);
                myParams[7].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertTSColumns", myParams);
                myColumn.columnId = Int32.Parse(myParams[7].Value.ToString());

                ht[dc.ColumnName.Trim()] = myColumn;


                columnOrder++;
            }

            _htTable = ht;

        }

        /// <summary>
        /// See if columns are defined in the db for this meta data set.
        /// </summary>
        public void GetTsColumnNames()
        {
            Hashtable tscn = new Hashtable();
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@TSid", SqlDbType.Int);
            myParams[0].Value = _tsId;
            myParams[1] = new SqlParameter("@ColumnDefinition", SqlDbType.VarChar);
            myParams[1].Value = _tsName;
            myParams[2] = new SqlParameter("@TSCount", SqlDbType.Int);
            myParams[2].Direction = ParameterDirection.Output;

            SqlDataReader rdr = SqlHelper.ExecuteReader(_connectionString, CommandType.StoredProcedure, "sproc_selectTSColumnNames", myParams);
            //int count = Int32.Parse(myParams[1].Value.ToString());


            ProcessColumnsDataReader(ref rdr, ref tscn);
            _htTable = tscn;
            rdr.Close();
        }

        /// <summary>
        /// Read through all the columns in the db and populate a hash we can use for a reference.
        /// The most important object in here is the _htTables hash.  That will have a hash for each table 
        /// in the db.
        /// </summary>
        /// <param name="rdr">data reader</param>
        /// <param name="ht">the hash </param>
        protected void ProcessColumnsDataReader(ref SqlDataReader rdr, ref Hashtable ht)
        {
            string firstDefinition = string.Empty;
            string secondDefinition = string.Empty;

            while (rdr.Read())
            {
                if (rdr["ColumnDefinition"] != null && firstDefinition != string.Empty && firstDefinition != rdr["ColumnDefinition"].ToString())
                    _htTable[firstDefinition.Trim()] = ht;

                column myColumn = new column();
                myColumn.columnId = (int)rdr["id"];
                myColumn.tsId = (int)rdr["TSid"];
                myColumn.type = rdr["ColumnType"].ToString();
                firstDefinition = myColumn.definition = rdr["ColumnDefinition"].ToString();
                myColumn.columnOrder = (int)rdr["ColumnOrder"];
                ht[rdr["ColumnName"].ToString().Trim()] = myColumn;
            }

            //rdr.Close();
        }

        public bool IsNumeric(Type type)
        {
            if (type == null)
            {
                return false;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return IsNumeric(Nullable.GetUnderlyingType(type));
                    }
                    return false;
            }
            return false;
        }

        public bool IsDate(Type type)
        {
            if (type == null)
            {
                return false;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.DateTime:
                    return true;
            }

            return false;
        }

        public bool IsDateString(string myString)
        {
            try
            {
                DateTime.Parse(myString);
                return true;
            }
            catch
            {
                return false;
            }

        }

        protected string CleanColumnName(ref SqlDataReader myDr)
        {
            return (myDr[0].ToString().Trim().Replace(' ', '_').Replace('(', '_').Replace(')', '_'));
        }

       

        protected void generateNewView(string name)
        {
            _viewCode = "CREATE VIEW \"vw_" + name + "\" AS (" + _viewCode + ")";
            SqlHelper.ExecuteScalar(_connectionString, CommandType.Text, _viewCode);
        }

        

        protected string getColumnNames(string tableName)
        {
            //Hashtable ht = (Hashtable)_htTable[tableName];
            StringBuilder sb = new StringBuilder();

            foreach (string key in _htTable.Keys)
            {
                sb.Append(key + ",");
            }

            return (sb.ToString().Substring(0, sb.ToString().Length - 1));

        }

        public string getTsColumnsForView()
        {
            SqlParameter[] myParams = new SqlParameter[2];
            myParams[0] = new SqlParameter("@TSid", SqlDbType.Int);
            myParams[0].Value = _tsId;
            myParams[1] = new SqlParameter("@ColumnDefinition", SqlDbType.VarChar);
            myParams[1].Value = _tsName;

            SqlDataReader myDr = SqlHelper.ExecuteReader(_connectionString, CommandType.StoredProcedure, "sproc_getTSColumns", myParams);
            int i = 1;
            StringBuilder startQuery = new StringBuilder();
            StringBuilder fullQuery = new StringBuilder();
            StringBuilder endQuery = new StringBuilder();


            //endQuery.Append(" where Q1.TimeSeriesId = Q2.TSID");
            startQuery.Append("select TimeSeriesId, TSName, Date_Time ");

            while (myDr.Read())
            {

                startQuery.Append("," + CleanColumnName(ref myDr));
                if (i == 1)
                {
                    fullQuery.Append("(select TSid as TimeSeriesId, TimeSeriesName as TSName,datetimeoccurred as Date_Time, value as " + CleanColumnName(ref myDr) + " from DisplayTimeSeriesNoPivot2 where intX = 1 and TSid = " + _tsId + " ) Q" + i + ",");
                    endQuery.Append(" where Q" + i + ".Date_Time=" + "Q" + (i + 1) + ".datetimeoccurred");
                }
                else
                {
                    fullQuery.Append("(select TSid, TimeSeriesName ,datetimeoccurred, value as " + CleanColumnName(ref myDr) + " from DisplayTimeSeriesNoPivot2 where intX = " + i + " and TSid = " + _tsId + " ) Q" + i + ",");

                    //if(i < _htTable.Count -2)
                    //    endQuery.Append(" and Q" + i + ".datetimeoccurred=" + "Q" + (i + 1) + ".datetimeoccurred");
                }

                i++;
            }

            int j = 2;

            //set up remaining where clause
            while (j < (i - 1))
            {
                endQuery.Append(" and Q" + j + ".datetimeoccurred=" + "Q" + (j + 1) + ".datetimeoccurred");
                j++;
            }


            myDr.Close();
            _viewCode = startQuery.ToString() + " from " + fullQuery.ToString().Substring(0, fullQuery.ToString().Length - 1) + endQuery.ToString();
            return (_viewCode);
        }

    }
}
