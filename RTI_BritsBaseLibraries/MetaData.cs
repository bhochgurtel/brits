﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using rti.common.cs.db.SqlServer;

namespace RTI_BritsBaseLibraries
{
    public class MetaData : Base
    {
        protected DataSet _mdExcel = null;
        protected bool _columnsExist = false;
        protected Hashtable _htTables = new Hashtable();

        /// <summary>
        /// Constructor to create and load the meata data
        /// </summary>
        /// <param name="connectionString">for the database we are using</param>
        /// <param name="metaDataName">The name of the meta data</param>
        /// <param name="metaDataFromExcel">Dataset representation of the the excel file</param>
        public MetaData(string connectionString, string metaDataName, DataSet metaDataFromExcel)
        {
            _connectionString = connectionString;
            _metaDataName = metaDataName;
            _mdExcel = metaDataFromExcel;
            Process();
        }

        /// <summary>
        /// Constructor to create and load the meata data
        /// </summary>
        /// <param name="connectionString">for the database we are using</param>
        /// <param name="metaDataId">The database id for the meta data in case it is known</param>
        /// <param name="metaDataFromExcel">Dataset representation of the the excel file</param>
        public MetaData(string connectionString, int metaDataId, DataSet metaDataFromExcel)
        {
            _connectionString = connectionString;
            _metaDataId = metaDataId;
            _mdExcel = metaDataFromExcel;
            Process();
        }

        /// <summary>
        /// Generic constructor -- in case the caller wants to load and process data seperately
        /// </summary>
        public MetaData()
        {
        }

        /// <summary>
        /// Lets load/save the metaData
        /// </summary>
        public virtual void Process()
        {
            Hashtable ht = new Hashtable();
            //does the meta data and the column count exist
            DoesMdExist();

            if (!this._metaDataExist)
                CreateMd();

            if (this._columnsExist)
                GetMdColumnNames();
            else
            {
                //create and gather the meta data
                CreateMdColumnNames();
                //now the data to the db
                SendColumnData();
            }
        }

        /// <summary>
        /// Create the meta data name in the database
        /// </summary>
        protected virtual void CreateMd()
        {
            if (_metaDataName == string.Empty)
                throw new Exception("Must provide a meta data name");
            
            foreach(DataTable dt in _mdExcel.Tables)
                 CreateMDEntry(dt.TableName);

        }

        protected virtual void CreateMDEntry(string tableName)
        {
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@MDName", SqlDbType.NVarChar);
            myParams[0].Value = tableName;
            myParams[1] = new SqlParameter("@GroupName", SqlDbType.NVarChar);
            myParams[1].Value = _metaDataName;
            myParams[2] = new SqlParameter("@MDid", SqlDbType.Int);
            myParams[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertMDName", myParams);
            _metaDataId = Int32.Parse(myParams[2].Value.ToString());
        }

        /// <summary>
        /// Create the Columns and Column names in the db
        /// </summary>
        protected virtual void CreateMdColumnNames()
        {
            if (this._mdExcel.Tables.Count == 0)
                throw new Exception("No data found in Excel file");

            foreach (DataTable dt in _mdExcel.Tables)
            {
                string name = dt.TableName;
                int columnOrder = 1;
                Hashtable ht = new Hashtable();
                foreach (DataColumn dc in dt.Columns)
                {
                    column myColumn = new column();
                    myColumn.type = dc.DataType.ToString();
                    myColumn.definition = dc.ColumnName;
                    myColumn.metaDataId = _metaDataId;
                    myColumn.columnOrder = columnOrder;


                    SqlParameter[] myParams = new SqlParameter[6];
                    myParams[0] = new SqlParameter("@name", SqlDbType.NVarChar, 50);
                    myParams[0].Value = dc.ColumnName.ToString();
                    myParams[1] = new SqlParameter("@type", SqlDbType.NVarChar, 50);
                    myParams[1].Value = dc.DataType.ToString();
                    myParams[2] = new SqlParameter("@definition", SqlDbType.NVarChar, 50);
                    myParams[2].Value = dt.TableName.ToString();
                    myParams[3] = new SqlParameter("@columnorder", SqlDbType.Int);
                    myParams[3].Value = columnOrder;
                    myParams[4] = new SqlParameter("@metadatanameid", SqlDbType.Int);
                    myParams[4].Value = _metaDataId;
                    myParams[5] = new SqlParameter("@id", SqlDbType.Int);
                    myParams[5].Direction = ParameterDirection.Output;

                    SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertMDColumnNames", myParams);
                    myColumn.columnId = Int32.Parse(myParams[5].Value.ToString());

                    ht[dc.ColumnName] = myColumn;


                    columnOrder++;
                }

                _htTables[dt.TableName] = ht;
            }
        }

        /// <summary>
        /// See if columns are defined in the db for this meta data set.
        /// </summary>
        public virtual void GetMdColumnNames()
        {
            Hashtable mdcn = new Hashtable();
            SqlParameter[] myParams = new SqlParameter[2];
            myParams[0] = new SqlParameter("@MDid", SqlDbType.Int);
            myParams[0].Value = _metaDataId;
            myParams[1] = new SqlParameter("@MDCount", SqlDbType.Int);
            myParams[1].Direction = ParameterDirection.Output;

            SqlDataReader rdr = SqlHelper.ExecuteReader(_connectionString, CommandType.StoredProcedure, "sproc_getMDColumnNames", myParams);
            //int count = Int32.Parse(myParams[1].Value.ToString());


            ProcessColumnsDataReader(ref rdr, ref mdcn);

            rdr.Close();
        }

        public virtual void SendColumnData()
        {
            foreach (DataTable dt in _mdExcel.Tables)
            {
                string name = dt.TableName;
                int x = 1;
                int y = 1;
                Hashtable ht = new Hashtable();
                foreach (DataRow dr in dt.Rows)
                {
                    x = 1;
                    Hashtable tableDefinition = (Hashtable)this._htTables[name];
                    foreach (DataColumn dc in dt.Columns)
                    {
                        ht = (Hashtable)_htTables[name];
                        column myColumn = (column)ht[dc.ColumnName];


                        SqlParameter[] myParams = new SqlParameter[7];
                        myParams[0] = new SqlParameter("@mdid", SqlDbType.Int);
                        myParams[0].Value = myColumn.metaDataId;
                        myParams[1] = new SqlParameter("@cid", SqlDbType.NVarChar, 50);
                        myParams[1].Value = myColumn.columnId;
                        myParams[2] = new SqlParameter("@columnName", SqlDbType.NVarChar, 50);
                        myParams[2].Value = dc.ColumnName;
                        myParams[3] = new SqlParameter("@value", SqlDbType.NVarChar, dr[dc.ColumnName].ToString().Length);
                        myParams[3].Value = dr[dc.ColumnName].ToString();
                        myParams[4] = new SqlParameter("@sortX", SqlDbType.Int);
                        myParams[4].Value = x;
                        myParams[5] = new SqlParameter("@sortY", SqlDbType.Int);
                        myParams[5].Value = y;
                        myParams[6] = new SqlParameter("@id", SqlDbType.Int);
                        myParams[6].Direction = ParameterDirection.Output;

                        SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "sproc_insertMDDefinition", myParams);
                        myColumn.columnId = Int32.Parse(myParams[5].Value.ToString());
                        x++;
                    }

                    y++;
                }
            }
        }

        public virtual string getMetaDataColumnsForView()
        {
            SqlParameter[] myParams = new SqlParameter[1];
            myParams[0] = new SqlParameter("@mdid", SqlDbType.Int);
            myParams[0].Value = _metaDataId;


            SqlDataReader myDr = SqlHelper.ExecuteReader(_connectionString, CommandType.StoredProcedure, "sproc_getMetaDataColumns", myParams);
            int i = 1;
            StringBuilder startQuery = new StringBuilder();
            StringBuilder fullQuery = new StringBuilder();
            StringBuilder endQuery = new StringBuilder();


            //endQuery.Append(" where Q1.TimeSeriesId = Q2.TSID");
            startQuery.Append("select  metadataid ");

            while (myDr.Read())
            {

                //startQuery.Append("," + CleanColumnName(ref myDr));
                if (i == 1)
                {
                    fullQuery.Append("(select id as metadataid, value as " + CleanColumnName(ref myDr) + ",sortY from vw_showMetaDataTablesWithSort where sortX = 1 and id = " + this._metaDataId + " ) Q" + i + ",");
                    endQuery.Append(" where Q" + i + ".metadataid=" + "Q" + (i + 1) + ".id");
                    endQuery.Append(" and Q" + i + ".sortY=" + "Q" + (i + 1) + ".sortY");
                }
                else
                {
                    fullQuery.Append("(select id, value as " + CleanColumnName(ref myDr) + ",sortY from vw_showMetaDataTablesWithSort where sortX = " + i + " and id = " + this._metaDataId + " ) Q" + i + ",");

                }
                startQuery.Append("," + CleanColumnName(ref myDr));

                i++;
            }


            int j = 2;

            //set up remaining where clause
            while (j < (i - 1))
            {
                endQuery.Append(" and Q" + j + ".id=" + "Q" + (j + 1) + ".id");
                endQuery.Append(" and Q" + j + ".sortY=" + "Q" + (j + 1) + ".sortY");
                j++;
            }


            myDr.Close();
            _viewCode = startQuery.ToString() + " from " + fullQuery.ToString().Substring(0, fullQuery.ToString().Length - 1) + endQuery.ToString();
            return (_viewCode);

        }

        public virtual void CreateViewForTimeSeriesMetaData()
        {
            if (this._viewCode == string.Empty)
                getMetaDataColumnsForView();

            generateNewView("meta_" + _metaDataName);
        }
    }
}
